import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function calcPercOnDate (freq, lastDate, date) {
  if (!freq || !lastDate) return 0
  const value = Math.floor(100 - ((1 / freq) * ((date - lastDate) / 86400000)) * 100)
  return value < 0 ? 0 : value
}

function save (state) {
  localStorage.setItem('state', JSON.stringify(state))
}

function load () {
  return JSON.parse(localStorage.getItem('state')) || null
}

export default new Vuex.Store({
  state: {
    relativeDate: new Date().valueOf(),
    data: {
      ext: {
        frequency: 2,
        lastDate: null
      },
      int: {
        frequency: 5,
        lastDate: null
      },
      sensi: {
        frequency: 1,
        lastDate: null
      },
      aspi: {
        frequency: 14,
        lastDate: null
      },
      bed: {
        frequency: 14,
        lastDate: null
      },
      dust: {
        frequency: 21,
        lastDate: null
      },
      pressage: {
        frequency: 14,
        lastDate: null
      },
      bathroom: {
        frequency: 21,
        lastDate: null
      }
    }
  },
  mutations: {
    set (state, type) {
      state.data[type].lastDate = new Date().valueOf()
      save(state.data)
    },
    setState (state, newData) {
      state.data = newData
    },
    updateTime (state, date) {
      state.relativeDate = date
    }
  },
  actions: {
    load ({ commit }) {
      const data = load()
      if (!data) return
      commit('updateTime', new Date().valueOf())
      commit('setState', data)

      setInterval(() => {
        commit('updateTime', new Date().valueOf())
      }, 1800000)
    },
    doneStuff ({ commit, state }, type) {
      commit('set', type)
      save(state.data)
    },
    saveSettings ({ commit }, settings) {
      commit('setState', settings)
      save(settings)
    }
  },
  getters: {
    extPlantWaterPercentage (state) {
      if (!state.data || !state.data.ext) return 0
      return calcPercOnDate(state.data.ext.frequency, state.data.ext.lastDate, state.relativeDate)
    },
    intPlantWaterPercentage (state) {
      if (!state.data || !state.data.int) return 0
      return calcPercOnDate(state.data.int.frequency, state.data.int.lastDate, state.relativeDate)
    },
    sensiPlantWaterPercentage (state) {
      if (!state.data || !state.data.sensi) return 0
      return calcPercOnDate(state.data.sensi.frequency, state.data.sensi.lastDate, state.relativeDate)
    },
    aspiPercentage (state) {
      if (!state.data || !state.data.aspi) return 0
      return calcPercOnDate(state.data.aspi.frequency, state.data.aspi.lastDate, state.relativeDate)
    },
    dustPercentage (state) {
      if (!state.data || !state.data.dust) return 0
      return calcPercOnDate(state.data.dust.frequency, state.data.dust.lastDate, state.relativeDate)
    },
    bedPercentage (state) {
      if (!state.data || !state.data.bed) return 0
      return calcPercOnDate(state.data.bed.frequency, state.data.bed.lastDate, state.relativeDate)
    },
    pressagePercentage (state) {
      if (!state.data || !state.data.pressage) return 0
      return calcPercOnDate(state.data.pressage.frequency, state.data.pressage.lastDate, state.relativeDate)
    },
    bathroomPercentage (state) {
      if (!state.data || !state.data.bathroom) return 0
      return calcPercOnDate(state.data.bathroom.frequency, state.data.bathroom.lastDate, state.relativeDate)
    }
  },
  modules: {
  }
})
